(*****************************************************************************)
(*                                                                           *)
(* Open Source License                                                       *)
(* Copyright (c) 2022 Nomadic Labs, <contact@nomadic-labs.com>               *)
(*                                                                           *)
(* Permission is hereby granted, free of charge, to any person obtaining a   *)
(* copy of this software and associated documentation files (the "Software"),*)
(* to deal in the Software without restriction, including without limitation *)
(* the rights to use, copy, modify, merge, publish, distribute, sublicense,  *)
(* and/or sell copies of the Software, and to permit persons to whom the     *)
(* Software is furnished to do so, subject to the following conditions:      *)
(*                                                                           *)
(* The above copyright notice and this permission notice shall be included   *)
(* in all copies or substantial portions of the Software.                    *)
(*                                                                           *)
(* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR*)
(* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,  *)
(* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL   *)
(* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER*)
(* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING   *)
(* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER       *)
(* DEALINGS IN THE SOFTWARE.                                                 *)
(*                                                                           *)
(*****************************************************************************)
open Tezos_crypto_dal
include Dal_cryptobox

type slot = bytes

type slot_header = commitment

let slot_header_encoding = Commitment.encoding

type parameters = {
  redundancy_factor : int;
  segment_size : int;
  slot_size : int;
  number_of_shards : int;
}

type error += Cryptobox_initialisation_failed of string

let () =
  register_error_kind
    `Permanent
    ~id:"dal.node.cryptobox.initialisation_failed"
    ~title:"Cryptobox initialisation failed"
    ~description:"Unable to initialise the cryptobox parameters"
    ~pp:(fun ppf msg ->
      Format.fprintf
        ppf
        "Unable to initialise the cryptobox parameters. Reason: %s"
        msg)
    Data_encoding.(obj1 (req "error" string))
    (function Cryptobox_initialisation_failed str -> Some str | _ -> None)
    (fun str -> Cryptobox_initialisation_failed str)

let init unsafe_srs cctxt (module Plugin : Dal_constants_plugin.T) =
  let open Lwt_result_syntax in
  let* Plugin.{redundancy_factor; segment_size; slot_size; number_of_shards} =
    Plugin.get_constants cctxt#chain cctxt#block cctxt
  in
  let parameters =
    {redundancy_factor; segment_size; slot_size; number_of_shards}
  in
  let* initialisation_parameters =
    if unsafe_srs then
      return
      @@ Internal_for_tests.initialisation_parameters_from_slot_size ~slot_size
    else
      let*? g1_path, g2_path = Tezos_base.Dal_srs.find_trusted_setup_files () in
      initialisation_parameters_from_files ~g1_path ~g2_path
  in
  let*? () = load_parameters initialisation_parameters in
  let* dal_constants =
    match
      make ~redundancy_factor ~segment_size ~slot_size ~number_of_shards
    with
    | Ok cryptobox -> return cryptobox
    | Error (`Fail msg) -> fail [Cryptobox_initialisation_failed msg]
  in
  return @@ (dal_constants, parameters)
